Facebook Social Sharer
=========
Facebook Social Sharer

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer require --prefer-dist oks/yii2-facebooksharer "dev-master"
```

or add

```
"oks/yii2-facebooksharer": "dev-master"
```

to the require section of your `composer.json` file.
